package com.diegopizzo.githubstargazers.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.diegopizzo.githubstargazers.R;
import com.diegopizzo.githubstargazers.ui.stargazersfragment.StargazersFragment;

import static com.diegopizzo.githubstargazers.ui.stargazersfragment.StargazersFragment.TAG_STARGAZERS_FRAGMENT;

/**
 * Essendo un'activity molto semplice non è stato implementato il pattern MVP ma
 * ovviamente anche nelle activities è opportuno implementare questo pattern come
 * è stato fatto nel fragment.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (null == savedInstanceState) {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, StargazersFragment.newInstance(savedInstanceState), TAG_STARGAZERS_FRAGMENT)
                    .commit();
        }
    }
}
