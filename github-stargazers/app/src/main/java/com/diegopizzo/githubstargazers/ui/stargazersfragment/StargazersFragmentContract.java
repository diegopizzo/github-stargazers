package com.diegopizzo.githubstargazers.ui.stargazersfragment;

import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;
import com.diegopizzo.githubstargazers.config.mvp.MvpPresenter;
import com.diegopizzo.githubstargazers.config.mvp.MvpView;

import java.util.List;

/**
 * Created by diegopizzo on 30/11/2017.
 */

class StargazersFragmentContract {

    interface View extends MvpView {
        void setDataOnRecyclerViewFirstPage(List<GitHubUser> gitHubUsers);

        void setDataOnRecyclerViewNextPages(List<GitHubUser> gitHubUsers);

        void hideUserInputView();
    }

    interface Presenter extends MvpPresenter {
        void getStargazersUsers(String owner, String repo, int page);

        void resumeGitHubUserList(final String owner, final String repo, final int pages);
    }
}
