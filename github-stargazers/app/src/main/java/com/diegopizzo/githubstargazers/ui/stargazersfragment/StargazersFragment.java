package com.diegopizzo.githubstargazers.ui.stargazersfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.diegopizzo.githubstargazers.R;
import com.diegopizzo.githubstargazers.business.network.model.GitHubUser;
import com.diegopizzo.githubstargazers.config.GitHubStargazersApplication;
import com.diegopizzo.githubstargazers.config.mvp.AbstractMvpFragment;
import com.diegopizzo.githubstargazers.ui.utils.EndlessRecyclerViewScrollListener;

import java.util.List;

import butterknife.BindView;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

/**
 * Created by diegopizzo on 30/11/2017.
 */

public class StargazersFragment extends AbstractMvpFragment<StargazersFragmentContract.Presenter>
        implements StargazersFragmentContract.View {

    public static final String TAG_STARGAZERS_FRAGMENT = "stargazersFragment";
    private static final String CARD_VISIBILITY_KEY = "cardVisibilityKey";
    private static final String PAGES_KEY = "pagesKey";
    private static final String PREVIOUS_ITEM_COUNT_KEY = "previousTotalItemCountKey";
    private static final String OWNER_KEY = "ownerKey";
    private static final String REPO_KEY = "repoKey";
    private static final String LAYOUT_POSITION_KEY = "layoutPositionKey";
    private static final int SPAN_COUNT = 2;
    @BindView(R.id.ownerEditText)
    EditText ownerEditText;
    @BindView(R.id.repoEditText)
    EditText repoEditText;
    @BindView(R.id.stargazersReclyclerView)
    RecyclerView stargazersRecyclerView;
    @BindView(R.id.searchButton)
    Button searchButton;
    @BindView(R.id.clearButton)
    Button clearButton;
    @BindView(R.id.floatingActionButton)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.user_input_include_layout)
    View userInputCard;
    View.OnClickListener clearButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            ownerEditText.setText("");
            repoEditText.setText("");
        }
    };

    private GridLayoutManager gridLayoutManager;
    private int pagesRead;
    private int previousTotalItemCount;
    private int layoutPosition;
    private EndlessRecyclerViewScrollListener scrollListener;
    private StargazersAdapter stargazersAdapter;

    View.OnClickListener fabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            showUserInputView();
            stargazersAdapter.clearRecyclerView();
        }
    };

    private AwesomeValidation mAwesomeValidation;

    View.OnClickListener searchButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if (mAwesomeValidation.validate()) {
                hideKeyboard();
                scrollListener.resetState();
                scrollListener.onLoadMore(1, 0, stargazersRecyclerView);
            }
        }
    };

    public static StargazersFragment newInstance(final Bundle bundle) {
        final StargazersFragment stargazersFragment = new StargazersFragment();
        if (bundle != null) {
            stargazersFragment.setArguments(bundle);
        }
        return stargazersFragment;
    }


    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stargazersAdapter = new StargazersAdapter(getContext());
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerView();
        searchButton.setOnClickListener(searchButtonClickListener);
        clearButton.setOnClickListener(clearButtonClickListener);
        floatingActionButton.setOnClickListener(fabClickListener);
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addValidations();
        if (savedInstanceState != null) {
            restoreData(savedInstanceState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        stargazersRecyclerView.post(() -> stargazersRecyclerView.scrollToPosition(layoutPosition));
    }

    private void restoreData(final Bundle savedInstanceState) {
        if (View.GONE == savedInstanceState.getInt(CARD_VISIBILITY_KEY)) {
            hideUserInputView();
            pagesRead = savedInstanceState.getInt(PAGES_KEY);
            previousTotalItemCount = savedInstanceState.getInt(PREVIOUS_ITEM_COUNT_KEY);
            scrollListener.setCurrentPage(pagesRead, previousTotalItemCount);
            presenter.resumeGitHubUserList(savedInstanceState.getString(OWNER_KEY),
                    savedInstanceState.getString(REPO_KEY), pagesRead);
            layoutPosition = savedInstanceState.getInt(LAYOUT_POSITION_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CARD_VISIBILITY_KEY, userInputCard.getVisibility());
        outState.putInt(PAGES_KEY, pagesRead);
        outState.putInt(PREVIOUS_ITEM_COUNT_KEY, previousTotalItemCount);
        outState.putString(OWNER_KEY, ownerEditText.getText().toString());
        outState.putString(REPO_KEY, repoEditText.getText().toString());
        outState.putInt(LAYOUT_POSITION_KEY, gridLayoutManager.findFirstVisibleItemPosition());
    }

    @Override
    public void setDataOnRecyclerViewFirstPage(final List<GitHubUser> gitHubUsers) {
        stargazersAdapter.setItemsFirstPage(gitHubUsers);
    }

    @Override
    public void setDataOnRecyclerViewNextPages(final List<GitHubUser> gitHubUsers) {
        stargazersAdapter.setItemsNextPages(gitHubUsers);
    }

    @Override
    public void hideUserInputView() {
        userInputCard.setVisibility(View.GONE);
        floatingActionButton.setVisibility(View.VISIBLE);
    }


    public void showUserInputView() {
        userInputCard.setVisibility(View.VISIBLE);
        floatingActionButton.setVisibility(View.INVISIBLE);
    }

    private void setRecyclerView() {
        // First param is number of columns and second param is orientation i.e Vertical or Horizontal
        gridLayoutManager = new GridLayoutManager(getContext(), SPAN_COUNT);
        // Attach the layout manager to the recycler userInputCard
        stargazersRecyclerView.setLayoutManager(gridLayoutManager);
        stargazersRecyclerView.setAdapter(stargazersAdapter);

        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(final int page, final int totalItemsCount, final RecyclerView view) {
                presenter.getStargazersUsers(ownerEditText.getText().toString(), repoEditText.getText().toString(), page);
                pagesRead = page;
                previousTotalItemCount = totalItemsCount;
            }

            @Override
            public void resetState() {
                super.resetState();
                pagesRead = 1;
                previousTotalItemCount = 0;
            }
        };
        stargazersRecyclerView.addOnScrollListener(scrollListener);
    }

    private void addValidations() {
        mAwesomeValidation = new AwesomeValidation(BASIC);
        mAwesomeValidation.addValidation(getActivity(), R.id.ownerTextInputLayout, RegexTemplate.NOT_EMPTY, R.string.mandatory_field);
        mAwesomeValidation.addValidation(getActivity(), R.id.repoTextInputLayout, RegexTemplate.NOT_EMPTY, R.string.mandatory_field);
    }

    private void hideKeyboard() {
        final View view = getActivity().getCurrentFocus();
        if (view != null) {
            final InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    @Override
    protected void inject() {
        DaggerStargazersFragmentComponent.builder()
                .applicationComponent(((GitHubStargazersApplication) getActivity().getApplication()).getApplicationComponent())
                .stargazersFragmentModule(new StargazersFragmentModule(this)).build().inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.stargazers_layout;
    }
}
